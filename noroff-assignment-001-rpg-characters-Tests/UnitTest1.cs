using noroff_assignment_001_rpg_characters.Character_Classes;
using noroff_assignment_001_rpg_characters.Equipment_Classes;
using noroff_assignment_001_rpg_characters.Custom_Exceptions;
using System;
using Xunit;

namespace noroff_assignment_001_rpg_characters_Tests
{
    public class UnitTest1
    {
        // Character attribute and level tests

        [Fact]
        public void Test_001_Level_1_Mage_Total_Level()
        {
            // Arrange
            Final_Hero_Class levelOneTestMage = new Final_Hero_Class("levelOneTestMage", "mage");
            int expectedCharacterLevel = 1;

            // Act
            int returnedCharacterLevel = levelOneTestMage.TotalLevel;

            // Assert
            Assert.Equal(expectedCharacterLevel, returnedCharacterLevel);
        }

        [Fact]
        public void Test_002_Level_1_Ranger_Total_Level()
        {
            // Arrange
            Final_Hero_Class levelOneTestRanger = new Final_Hero_Class("levelOneTestRanger", "ranger");
            int expectedCharacterLevel = 1;

            // Act
            int returnedCharacterLevel = levelOneTestRanger.TotalLevel;

            // Assert
            Assert.Equal(expectedCharacterLevel, returnedCharacterLevel);
        }

        [Fact]
        public void Test_003_Level_1_Rouge_Total_Level()
        {
            // Arrange
            Final_Hero_Class levelOneTestRouge = new Final_Hero_Class("levelOneTestRouge", "rouge");
            int expectedCharacterLevel = 1;

            // Act
            int returnedCharacterLevel = levelOneTestRouge.TotalLevel;

            // Assert
            Assert.Equal(expectedCharacterLevel, returnedCharacterLevel);
        }

        [Fact]
        public void Test_004_Level_1_Warrior_Total_Level()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            int expectedCharacterLevel = 1;

            // Act
            int returnedCharacterLevel = levelOneTestWarrior.TotalLevel;

            // Assert
            Assert.Equal(expectedCharacterLevel, returnedCharacterLevel);
        }

        [Fact]
        public void Test_005_Level_2_Mage_Total_Strenght_Dexterity_Intelligence()
        {
            // Arrange
            Final_Hero_Class levelTwoTestMage = new Final_Hero_Class("levelTwoTestMage", "mage");
            levelTwoTestMage.LevelUp();
            int characterBaseStrength = 1;
            int characterLevelUpStrengthModifier = 1;
            int expectedCharacterTotalStrength = characterBaseStrength + characterLevelUpStrengthModifier;
            int characterBaseDexterity = 1;
            int characterLevelUpDexterityModifier = 1;
            int expectedCharacterTotalDexterity = characterBaseDexterity + characterLevelUpDexterityModifier;
            int characterBaseIntelligence = 8;
            int characterLevelUpIntelligenceModifier = 5;
            int expectedCharacterTotalIntelligence = characterBaseIntelligence + characterLevelUpIntelligenceModifier;
            int[] expectedCharacterTotalAttributes = { expectedCharacterTotalStrength, expectedCharacterTotalDexterity, expectedCharacterTotalIntelligence };

            // Act
            int[] returnedCharacterTotalAttributes = { levelTwoTestMage.TotalStrength, levelTwoTestMage.TotalDexterity, levelTwoTestMage.TotalIntelligence };

            // Assert
            Assert.Equal(expectedCharacterTotalAttributes, returnedCharacterTotalAttributes);
        }

        [Fact]
        public void Test_006_Level_2_Ranger_Total_Strenght_Dexterity_Intelligence()
        {
            // Arrange
            Final_Hero_Class levelTwoTestRanger = new Final_Hero_Class("levelTwoTestRanger", "ranger");
            levelTwoTestRanger.LevelUp();
            int characterBaseStrength = 1;
            int characterLevelUpStrengthModifier = 1;
            int expectedCharacterTotalStrength = characterBaseStrength + characterLevelUpStrengthModifier;
            int characterBaseDexterity = 7;
            int characterLevelUpDexterityModifier = 5;
            int expectedCharacterTotalDexterity = characterBaseDexterity + characterLevelUpDexterityModifier;
            int characterBaseIntelligence = 1;
            int characterLevelUpIntelligenceModifier = 1;
            int expectedCharacterTotalIntelligence = characterBaseIntelligence + characterLevelUpIntelligenceModifier;
            int[] expectedCharacterTotalAttributes = { expectedCharacterTotalStrength, expectedCharacterTotalDexterity, expectedCharacterTotalIntelligence };

            // Act
            int[] returnedCharacterTotalAttributes = { levelTwoTestRanger.TotalStrength, levelTwoTestRanger.TotalDexterity, levelTwoTestRanger.TotalIntelligence };

            // Assert
            Assert.Equal(expectedCharacterTotalAttributes, returnedCharacterTotalAttributes);
        }

        [Fact]
        public void Test_007_Level_2_Rouge_Total_Strenght_Dexterity_Intelligence()
        {
            // Arrange
            Final_Hero_Class levelTwoTestRouge = new Final_Hero_Class("levelTwoTestRouge", "rouge");
            levelTwoTestRouge.LevelUp();
            int characterBaseStrength = 2;
            int characterLevelUpStrengthModifier = 1;
            int expectedCharacterTotalStrength = characterBaseStrength + characterLevelUpStrengthModifier;
            int characterBaseDexterity = 6;
            int characterLevelUpDexterityModifier = 4;
            int expectedCharacterTotalDexterity = characterBaseDexterity + characterLevelUpDexterityModifier;
            int characterBaseIntelligence = 1;
            int characterLevelUpIntelligenceModifier = 1;
            int expectedCharacterTotalIntelligence = characterBaseIntelligence + characterLevelUpIntelligenceModifier;
            int[] expectedCharacterTotalAttributes = { expectedCharacterTotalStrength, expectedCharacterTotalDexterity, expectedCharacterTotalIntelligence };

            // Act
            int[] returnedCharacterTotalAttributes = { levelTwoTestRouge.TotalStrength, levelTwoTestRouge.TotalDexterity, levelTwoTestRouge.TotalIntelligence };

            // Assert
            Assert.Equal(expectedCharacterTotalAttributes, returnedCharacterTotalAttributes);
        }

        [Fact]
        public void Test_008_Level_2_Warrior_Total_Strenght_Dexterity_Intelligence()
        {
            // Arrange
            Final_Hero_Class levelTwoTestWarrior = new Final_Hero_Class("levelTwoTestWarrior", "warrior");
            levelTwoTestWarrior.LevelUp();
            int characterBaseStrength = 5;
            int characterLevelUpStrengthModifier = 3;
            int expectedCharacterTotalStrength = characterBaseStrength + characterLevelUpStrengthModifier;
            int characterBaseDexterity = 2;
            int characterLevelUpDexterityModifier = 2;
            int expectedCharacterTotalDexterity = characterBaseDexterity + characterLevelUpDexterityModifier;
            int characterBaseIntelligence = 1;
            int characterLevelUpIntelligenceModifier = 1;
            int expectedCharacterTotalIntelligence = characterBaseIntelligence + characterLevelUpIntelligenceModifier;
            int[] expectedCharacterTotalAttributes = { expectedCharacterTotalStrength, expectedCharacterTotalDexterity, expectedCharacterTotalIntelligence };

            // Act
            int[] returnedCharacterTotalAttributes = { levelTwoTestWarrior.TotalStrength, levelTwoTestWarrior.TotalDexterity, levelTwoTestWarrior.TotalIntelligence };

            // Assert
            Assert.Equal(expectedCharacterTotalAttributes, returnedCharacterTotalAttributes);
        }


        // Items and equipment tests

        [Fact]
        public void Test_009_Level_1_Warrior_Mediocre_Axe_Equipped_Weapon_Level_Too_High_Exception()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            string expectedExceptionMessage = "The character level is too low to equip weapon 'Mediocre axe'";
            string resultExceptionMessage = "";

            //Act
            try
            {
                levelOneTestWarrior.EquipWeapon("mediocre axe");
            }
            catch (Exception ex)
            {
                resultExceptionMessage = ex.Message;

            }
            // Assert
            Assert.Equal(expectedExceptionMessage, resultExceptionMessage);
        }

        [Fact]
        public void Test_010_Level_1_Warrior_Mediocre_Plate_Armor_Equipped_Armor_Level_Too_High_Exception()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            string expectedExceptionMessage = "The character level is too low to equip armor 'Mediocre plate armor'";
            string resultExceptionMessage = "";

            //Act
            try
            {
                levelOneTestWarrior.EquipBody("mediocre plate armor");
            }
            catch (Exception ex)
            {
                resultExceptionMessage = ex.Message;

            }
            // Assert
            Assert.Equal(expectedExceptionMessage, resultExceptionMessage);
        }

        [Fact]
        public void Test_011_Level_1_Warrior_Old_Bow_Equipped_Weapon_Not_Equippable_By_Hero_Class_Exception()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            string expectedExceptionMessage = "The weapon 'Old bow' cannot be used by this character class";
            string resultExceptionMessage = "";

            //Act
            try
            {
                levelOneTestWarrior.EquipWeapon("old bow");
            }
            catch (Exception ex)
            {
                resultExceptionMessage = ex.Message;

            }
            // Assert
            Assert.Equal(expectedExceptionMessage, resultExceptionMessage);
        }

        [Fact]
        public void Test_012_Level_1_Warrior_Old_Cloth_Cap_Equipped_Armor_Not_Equippable_By_Hero_Class_Exception()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            string expectedExceptionMessage = "The armor 'Old cloth cap' cannot be used by this character class";
            string resultExceptionMessage = "";

            //Act
            try
            {
                levelOneTestWarrior.EquipHead("old cloth cap");
            }
            catch (Exception ex)
            {
                resultExceptionMessage = ex.Message;

            }
            // Assert
            Assert.Equal(expectedExceptionMessage, resultExceptionMessage);
        }

        [Fact]
        public void Test_013_Level_1_Warrior_Old_Sword_Equipped_Success()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");

            // Act
            try
            {
                levelOneTestWarrior.EquipWeapon("old sword");
                // Assert
                Assert.True(true);                
            }
            catch(Exception ex)
            {
                // Assert
                Assert.True(false, ex.Message);
            }
        }

        [Fact]
        public void Test_014_Level_1_Warrior_Old_Mail_Pants_Equipped_Success()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");

            // Act
            try
            {
                levelOneTestWarrior.EquipLegs("old mail pants");
                // Assert
                Assert.True(true);
            }
            catch (Exception ex)
            {
                // Assert
                Assert.True(false, ex.Message);
            }
        }

        [Fact]
        public void Test_015_Level_1_Warrior_Unequipped_Character_Damage()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            double noWeaponEquippedBaseDPS = 1;
            double levelOneWarriorStrength = 5;
            double expectedCharacterDamage = (double)Math.Round(noWeaponEquippedBaseDPS * (1 + levelOneWarriorStrength / 100 ), 1);

            // Act
            double returnedCharacterDamage = levelOneTestWarrior.CharacterDamage;

            // Assert
            Assert.Equal(expectedCharacterDamage, returnedCharacterDamage);

        }

        [Fact]
        public void Test_016_Level_1_Warrior_Old_Axe_Equipped_Character_Damage()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            levelOneTestWarrior.EquipWeapon("old axe");
            double levelOneWarriorStrength = 5;
            double oldAxeItemPower = 4;
            double oldAxeItemAttackSpeed = 0.9;
            double expectedCharacterDamage = (double)Math.Round((oldAxeItemPower * oldAxeItemAttackSpeed) * (1 +(levelOneWarriorStrength / 100)), 1);

            // Act
            double returnedCharacterDamage = levelOneTestWarrior.CharacterDamage;

            // Assert
            Assert.Equal(expectedCharacterDamage, returnedCharacterDamage);
        }

        [Fact]
        public void Test_017_Level_1_Warrior_Old_Axe_Plus_Old_Plate_Armor_Equipped_Character_Damage()
        {
            // Arrange
            Final_Hero_Class levelOneTestWarrior = new Final_Hero_Class("levelOneTestWarrior", "warrior");
            levelOneTestWarrior.EquipWeapon("old axe");
            levelOneTestWarrior.EquipBody("old plate armor");
            double levelOneWarriorStrength = 5;
            double oldAxeItemPower = 4;
            double oldAxeItemAttackSpeed = 0.9;
            double oldPlateArmorItemPower = 4;
            double expectedCharacterDamage = (double)Math.Round((double)Math.Round((oldAxeItemPower * oldAxeItemAttackSpeed) * (1 + ((levelOneWarriorStrength + oldPlateArmorItemPower) / 100)), 1));

            // Act
            double returnedCharacterDamage = levelOneTestWarrior.CharacterDamage;

            // Assert
            Assert.Equal(expectedCharacterDamage, returnedCharacterDamage);
        }
    }
}