# noroff-assignment-001-rpg-characters

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A solution for the Noroff academy assignment 001 rpg-characters

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Install

```
Clone repository and open solution in Visual Studio
  
```

## Usage

```
The methods provided are to be used in the try block in Program.cs  
  
  
Heroes:  
  
You can create a hero, do a level up and equip or unequip him with items  
See "Hero Creation" and "Public methods" sections further below for detailed informations  

There is no way to level a hero down  

Every hero has a primary attribute based on his hero class  

This primary attribute is used to calculate the damage output of the character  

The primary attributes for each hero class are:  
  
	mage : Intelligence  
	ranger : Dexterity  
	rouge : Dexterity  
	warrior : Strength  
  
   
Items:  
  
Every item has individual stats  
  
There is no way to create new items  
  
Every item has a minimum level, at when it can be used  
  
In general, old items can be used at level 1, mediocre items can be used at level 3 and new items can be used at level 5  
  
Armor items add a bonus to the primary attribute of the hero  
  
Weapon items add a DPS Value to the damage output of the character  
  
The items are also restricted to different hero classes  
  
  
Armor Restrictions:  
  
	mage can equip cloth  
	ranger can equip leather and mail  
	rouge can equip leather and mail  
	warrior can equip mail and plate  
  
    
Weapon Restrictions:  
  
	mage can equip staff and wand  
	ranger can equip bow  
	rouge can equip dagger and sword  
	warrior can equip axe, hammer and sword  
  
  
Every item is restricted to a specific item slot of the hero  
  
  
Available item slots:  
  
	headSlot  
	bodySlot  
	legsSlot  
	weaponSlot  
  
To check, which item is to be equiped at which slot, see the "Available items" list further below  
  
    
Hero Creation:  
  
Creating a hero:  
  
	Final_Hero_Class heroName = new Final_Hero_Class("name", "class");  
	  
	Example:  
  
		Final_Hero_Class heroHerkules = new Final_Hero_Class("Herkules", "warrior");  
  
  
Parameter Info:  
  
	The name is up to you  
	  
	Acvailable classes are:  
	  
		mage  
		ranger  
		rouge  
		warrior  
  
  
Public Methods:  

	LevelUp();  

	EquipWeapon();  

	UnequipWeapon();  

	EquipHead();  

	UnequipHead();  

	EquipBody();  

	UnequipBody();  

	EquipLegs();  

	UnequipLegs();  
  
	PrintBasicStats();  

	PrintVerboseStats();  
  
  
Usage Examples:  
	  
	heroHerkules.EquipBody("Old mail coat");  
	  
	heroHerkules.UnequipBody();  
	  
	heroHerkules.EquipWeapon("Old sword");  
	  
	heroHerkules.UnequipWeapon();  
	  
	heroHerkules.LevelUp();  

	heroHerkules.PrintBasicStats();  

	heroHerkules.PrintVerboseStats();  

  
Available Items:  

Weapon Items:  

	"Old axe"  
	"Mediocre axe"  
	"New axe"  

	"Old bow"  
	"Mediocre bow"  
	"New bow"  

	"Old dagger"  
	"Mediocre dagger"  
	"New dagger"  

	"Old hammer"  
	"Mediocre hammer"  
	"New hammer"  

	"Old staff"  
	"Mediocre staff"  
	"New staff"  

	"Old sword"  
	"Mediocre sword"  
	"New sword"  

	"Old wand"  
	"Mediocre wand"  
	"New wand"  
  
  
Armor items:  
  
	Head armor:  

		"Old cloth cap"  
		"Mediocre cloth cap"  
		"New cloth cap"  

		"Old leather hat"  
		"Mediocre leather hat"  
		"New leather hat"  

		"Old mail hood"  
		"Mediocre mail hood"  
		"New mail hood"  

		"Old plate helmet"  
		"Mediocre plate helmet"  
		"New plate helmet"  

	Body armor:  

		"Old cloth robe"  
		"Mediocre cloth robe"  
		"New cloth robe"  

		"Old leather cloak"  
		"Mediocre leather cloak"  
		"New leather cloak"  

		"Old mail coat"  
		"Mediocre mail coat"  
		"New mail coat"  

		"Old plate armor"  
		"Mediocre plate armor"  
		"New plate armor"  

	Legs armor:  

		"Old cloth pants"  
		"Mediocre cloth pants"  
		"New cloth pants"  

		"Old leather pants"  
		"Mediocre leather pants"  
		"New leather pants"  

		"Old mail pants"  
		"Mediocre mail pants"  
		"New mail pants"  

		"Old plate leg armor"  
		"Mediocre plate leg armor"  
		"New plate leg armor"  
  
  
Additional information:  

	There is an usage example prepared in program.cs. To use it, just uncomment it.  
	Also unit tests are provided in UnitTest1.cs in the noroff-assignment-001-rpg-characters-Tests. As every test is named by it's purpose, it should be self explanatory what they test for. 
	
```

## Maintainers

Daniel Ruegenberg


## License

MIT © 2022 Daniel Ruegenberg
