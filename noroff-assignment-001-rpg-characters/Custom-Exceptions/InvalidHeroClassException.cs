﻿using System;

namespace noroff_assignment_001_rpg_characters.Custom_Exceptions
{
    [Serializable]
    public class InvalidHeroClassException : Exception
    {
        public InvalidHeroClassException()
        {
        }
        public InvalidHeroClassException(string message)
            : base(message)
        {
        }
        public InvalidHeroClassException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
