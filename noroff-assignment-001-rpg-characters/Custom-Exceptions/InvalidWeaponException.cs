﻿using System;

namespace noroff_assignment_001_rpg_characters.Custom_Exceptions
{
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        { 
        }
        public InvalidWeaponException(string message)
            : base(message)
        { 
        }
        public InvalidWeaponException(string message, Exception innerException)
            : base(message, innerException)
        { 
        }
    }
}
