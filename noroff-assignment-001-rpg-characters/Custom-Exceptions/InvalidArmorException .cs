﻿using System;

namespace noroff_assignment_001_rpg_characters.Custom_Exceptions
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        { 
        }
        public InvalidArmorException(string message)
            : base(message)
        { 
        }
        public InvalidArmorException(string message, Exception innerException)
            : base(message, innerException)
        { 
        }
    }
}
