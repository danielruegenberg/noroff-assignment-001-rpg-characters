﻿using noroff_assignment_001_rpg_characters.Custom_Exceptions;
using System.Collections.Generic;

namespace noroff_assignment_001_rpg_characters.Equipment_Classes
{
    public class Armor_Class : Equipment_Base_Class
    {
        /// <summary>
        /// Takes in the information of the armor to be equipped, the necessary informations about the character that is going to equip it and checks, if the conditions are met to do that
        /// </summary>
        /// <param name="nameOfArmor"></param>
        /// <param name="levelOfChar"></param>
        /// <param name="usableArmorOfCharacter"></param>
        /// <param name="armorSlot"></param>
        /// <returns>string armorName, int armorPower</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public (string armorName, int armorPower) EquipArmor(string nameOfArmor, int levelOfChar, List<string> usableArmorOfCharacter, string armorSlot)
        {
            foreach (string armor in armorList.Keys)
            {   
                if (armor == nameOfArmor)
                {
                    ArmorEquipment singleArmor = (ArmorEquipment)armorList[armor];               
                    if (singleArmor.MinimumCharacterLevel <= levelOfChar)
                    {
                        bool itemValidForCharacterClass = false;
                        
                        foreach(string armorType in usableArmorOfCharacter)
                        {
                            if(singleArmor.MaterialType == armorType)
                            {
                                itemValidForCharacterClass = true;
                            }
                        }
                        if (itemValidForCharacterClass)
                        {
                            if(singleArmor.BodySlot == armorSlot)
                            {
                                int itemPowerValue = singleArmor.ItemPower;
                                return (singleArmor.ItemName, itemPowerValue);
                            } 
                            else 
                            {
                                throw new InvalidArmorException($"The item '{ singleArmor.ItemName }' cannot be equipped at { armorSlot }");
                            }                                            
                        } 
                        else 
                        {
                            throw new InvalidArmorException($"The armor '{ singleArmor.ItemName }' cannot be used by this character class");
                        }
                    } 
                    else
                    {
                        throw new InvalidArmorException($"The character level is too low to equip armor '{ singleArmor.ItemName }'");
                    }                 
                }
            }
            throw new InvalidArmorException($"The armor '{ nameOfArmor }' does not exist");
        }
    }
}
