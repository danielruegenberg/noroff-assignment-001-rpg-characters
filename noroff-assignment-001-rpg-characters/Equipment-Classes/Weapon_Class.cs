﻿using noroff_assignment_001_rpg_characters.Custom_Exceptions;
using System.Collections.Generic;

namespace noroff_assignment_001_rpg_characters.Equipment_Classes
{
    public class Weapon_Class : Equipment_Base_Class
    {
        /// <summary>
        /// Takes in the information of the weapon to be equipped, the necessary informations about the character that is going to equip it and checks, if the conditions are met to do that
        /// </summary>
        /// <param name="nameOfWeapon"></param>
        /// <param name="levelOfChar"></param>
        /// <param name="usableWeaponsOfCharacter"></param>
        /// <returns>string weaponName, int weaponPower, double weaponAttackSpeed</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public (string weaponName, int weaponPower, double weaponAttackSpeed) EquipWeapon(string nameOfWeapon, int levelOfChar, List<string> usableWeaponsOfCharacter)
        {
            foreach (string weapon in weaponsList.Keys)
            {
                if (weapon == nameOfWeapon)
                {
                    WeaponEquipment singleWeapon = (WeaponEquipment)weaponsList[weapon];
                    if ( singleWeapon.MinimumCharacterLevel <= levelOfChar )
                    {
                        bool itemValidForCharacterClass = false;
                        foreach ( string weaponType in usableWeaponsOfCharacter )
                        {
                            if ( singleWeapon.WeaponType == weaponType )
                            {
                                itemValidForCharacterClass = true;
                            }
                        }
                        if ( itemValidForCharacterClass )
                        {
                            if ( singleWeapon.BodySlot == "weaponSlot" )
                            {
                                return ( singleWeapon.ItemName, singleWeapon.ItemPower, singleWeapon.AttackSpeed );
                            }
                            else
                            {
                                throw new InvalidWeaponException( $"The item '{ singleWeapon.ItemName }' cannot be equipped at weaponSlot" );
                            }
                        }
                        else
                        {
                            throw new InvalidWeaponException($"The weapon '{ singleWeapon.ItemName }' cannot be used by this character class");
                        }
                    }
                    else
                    {
                        throw new InvalidWeaponException($"The character level is too low to equip weapon '{ singleWeapon.ItemName }'");
                    }
                }
            }
            throw new InvalidWeaponException($"The weapon '{ nameOfWeapon }' does not exist");
        }
    }
}
