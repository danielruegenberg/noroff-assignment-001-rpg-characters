﻿using System.Collections.Generic;

namespace noroff_assignment_001_rpg_characters.Equipment_Classes
{
    abstract public class Equipment_Base_Class
    {
        public readonly Dictionary<string, object> armorList = new Dictionary<string, object>()
        {
            {"old cloth cap", new ArmorEquipment("Old cloth cap", 1, "cloth", "headSlot", 1)},
            {"mediocre cloth cap", new ArmorEquipment("Mediocre cloth cap", 2, "cloth", "headSlot", 3)},
            {"new cloth cap", new ArmorEquipment("New cloth cap", 3, "cloth", "headSlot", 5)},
            {"old leather hat", new ArmorEquipment("Old leather hat", 2, "leather", "headSlot", 1)},
            {"mediocre leather hat", new ArmorEquipment("Mediocre leather hat", 3, "leather", "headSlot", 3)},
            {"new leather hat", new ArmorEquipment("New leather hat", 4, "leather", "headSlot", 5)},
            {"old mail hood", new ArmorEquipment("Old mail hood", 3, "mail", "headSlot", 1)},
            {"mediocre mail hood", new ArmorEquipment("Mediocre mail hood", 4, "mail", "headSlot", 3)},
            {"new mail hood", new ArmorEquipment("New mail hood", 5, "mail", "headSlot", 5)},
            {"old plate helmet", new ArmorEquipment("Old plate helmet", 4, "plate", "headSlot", 1)},
            {"mediocre plate helmet", new ArmorEquipment("Mediocre plate helmet", 5, "plate", "headSlot", 3)},
            {"new plate helmet", new ArmorEquipment("New plate helmet", 6, "plate", "headSlot", 5)},

            {"old cloth robe", new ArmorEquipment("Old cloth robe", 2, "cloth", "bodySlot", 1)},
            {"mediocre cloth robe", new ArmorEquipment("Mediocre cloth robe", 3, "cloth", "bodySlot", 3)},
            {"new cloth robe", new ArmorEquipment("New cloth robe", 4, "cloth", "bodySlot", 5)},
            {"old leather cloak", new ArmorEquipment("Old leather cloak", 3, "leather", "bodySlot", 1)},
            {"mediocre leather cloak", new ArmorEquipment("Mediocre leather cloak", 4, "leather", "bodySlot", 3)},
            {"new leather cloak", new ArmorEquipment("New leather cloak", 5, "leather", "bodySlot", 5)},
            {"old mail coat", new ArmorEquipment("Old mail coat", 4, "mail", "bodySlot", 1)},
            {"mediocre mail coat", new ArmorEquipment("Mediocre mail coat", 5, "mail", "bodySlot", 3)},
            {"new mail coat", new ArmorEquipment("New mail coat", 6, "mail", "bodySlot", 5)},
            {"old plate armor", new ArmorEquipment("Old plate armor", 5, "plate", "bodySlot", 1)},
            {"mediocre plate armor", new ArmorEquipment("Mediocre plate armor", 6, "plate", "bodySlot", 3)},
            {"new plate armor", new ArmorEquipment("New plate armor", 7, "plate", "bodySlot", 5)},

            {"old cloth pants", new ArmorEquipment("Old cloth pants", 1, "cloth", "legsSlot", 1)},
            {"mediocre cloth pants", new ArmorEquipment("Mediocre cloth pants", 2, "cloth", "legsSlot", 3)},
            {"new cloth pants", new ArmorEquipment("New cloth pants", 3, "cloth", "legsSlot", 5)},
            {"old leather pants", new ArmorEquipment("Old leather pants", 2, "leather", "legsSlot", 1)},
            {"mediocre leather pants", new ArmorEquipment("Mediocre leather pants", 3, "leather", "legsSlot", 3)},
            {"new leather pants", new ArmorEquipment("New leather pants", 4, "leather", "legsSlot", 5)},
            {"old mail pants", new ArmorEquipment("Old mail pants", 3, "mail", "legsSlot", 1)},
            {"mediocre mail pants", new ArmorEquipment("Mediocre mail pants", 4, "mail", "legsSlot", 3)},
            {"new mail pants", new ArmorEquipment("New mail pants", 5, "mail", "legsSlot", 5)},
            {"old plate leg armor", new ArmorEquipment("Old plate armor", 4, "plate", "legsSlot", 1)},
            {"mediocre plate leg armor", new ArmorEquipment("Mediocre plate armor", 5, "plate", "legsSlot", 3)},
            {"new plate leg armor", new ArmorEquipment("New plate armor", 6, "plate", "legsSlot", 5)}
        };

        public readonly Dictionary<string, object> weaponsList = new Dictionary<string, object>()
        {
            {"old axe", new WeaponEquipment("Old axe", 4, 0.9, "axe", "weaponSlot", 1)},
            {"mediocre axe", new WeaponEquipment("Mediocre axe", 6, 0.9, "axe", "weaponSlot", 3)},
            {"new axe", new WeaponEquipment("New axe", 8, 0.9, "axe", "weaponSlot", 5)},

            {"old bow", new WeaponEquipment("Old bow", 5, 0.6, "bow", "weaponSlot", 1)},
            {"mediocre bow", new WeaponEquipment("Mediocre bow", 7, 0.6, "bow", "weaponSlot", 3)},
            {"new bow", new WeaponEquipment("New bow", 9, 0.6, "bow", "weaponSlot", 5)},

            {"old dagger", new WeaponEquipment("Old dagger", 2, 1.5, "dagger", "weaponSlot", 1)},
            {"mediocre dagger", new WeaponEquipment("Mediocre dagger", 4, 1.5, "dagger", "weaponSlot", 3)},
            {"new dagger", new WeaponEquipment("New dagger", 6, 1.5, "dagger", "weaponSlot", 5)},

            {"old hammer", new WeaponEquipment("Old hammer", 6, 0.7, "hammer", "weaponSlot", 1)},
            {"mediocre hammer", new WeaponEquipment("Mediocre hammer", 8, 0.7, "hammer", "weaponSlot", 3)},
            {"new hammer", new WeaponEquipment("New hammer", 10, 0.7, "hammer", "weaponSlot", 5)},

            {"old staff", new WeaponEquipment("Old staff", 10, 0.3, "staff", "weaponSlot", 1)},
            {"mediocre staff", new WeaponEquipment("Mediocre staff", 12, 0.3, "staff", "weaponSlot", 3)},
            {"new staff", new WeaponEquipment("New staff", 14, 0.3, "staff", "weaponSlot", 5)},

            {"old sword", new WeaponEquipment("Old sword", 3, 1.2, "sword", "weaponSlot", 1)},
            {"mediocre sword", new WeaponEquipment("Mediocre sword", 5, 1.2, "sword", "weaponSlot", 3)},
            {"new sword", new WeaponEquipment("New sword", 7, 1.2, "sword", "weaponSlot", 5)},

            {"old wand", new WeaponEquipment("Old wand", 2, 1.1, "wand", "weaponSlot", 1)},
            {"mediocre wand", new WeaponEquipment("Mediocre wand", 4, 1.1, "wand", "weaponSlot", 3)},
            {"new wand", new WeaponEquipment("New wand", 6, 1.1, "wand", "weaponSlot", 5)}
        };
        public class ArmorEquipment
        {
            private string itemName;
            private int itemPower;
            private string materialType;
            private string bodySlot;
            private int minimumCharacterLevel;
            /// <summary>
            /// The construcotr creates an object of type ArmorEquipment and passes in all information the object needs
            /// </summary>
            /// <param name="passItemName"></param>
            /// <param name="passItemPower"></param>
            /// <param name="passMaterialType"></param>
            /// <param name="passBodySlot"></param>
            /// <param name="passMinimumCharacterLevel"></param>
            public ArmorEquipment(string passItemName, int passItemPower, string passMaterialType, string passBodySlot, int passMinimumCharacterLevel)
            {
                itemName = passItemName;
                itemPower = passItemPower;
                materialType = passMaterialType;
                bodySlot = passBodySlot;
                minimumCharacterLevel = passMinimumCharacterLevel;
            }
            public string ItemName { get => itemName; }
            public int ItemPower { get => itemPower; }
            public string MaterialType { get => materialType; }
            public string BodySlot { get => bodySlot; }
            public int MinimumCharacterLevel { get => minimumCharacterLevel; }
        }
        public class WeaponEquipment
        {
            private string itemName;
            private int itemPower;
            private double attackSpeed;
            private string weaponType;
            private string bodySlot;
            private int minimumCharacterLevel;
            /// <summary>
            /// The construcotr creates an object of type WeaponEquipment and passes in all information the object needs
            /// </summary>
            /// <param name="passItemName"></param>
            /// <param name="passItemPower"></param>
            /// <param name="passAttackSpeed"></param>
            /// <param name="passWeaponType"></param>
            /// <param name="passBodySlot"></param>
            /// <param name="passMinimumCharacterLevel"></param>
            public WeaponEquipment(string passItemName, int passItemPower, double passAttackSpeed, string passWeaponType, string passBodySlot, int passMinimumCharacterLevel)
            {
                itemName = passItemName;
                itemPower = passItemPower;
                attackSpeed = passAttackSpeed;
                weaponType = passWeaponType;
                bodySlot = passBodySlot;
                minimumCharacterLevel = passMinimumCharacterLevel;
            }
            public string ItemName { get => itemName; }
            public int ItemPower { get => itemPower; }
            public double AttackSpeed { get => attackSpeed; set => attackSpeed = value; }
            public string WeaponType { get => weaponType; }
            public string BodySlot { get => bodySlot; }
            public int MinimumCharacterLevel { get => minimumCharacterLevel; }
        }
    }
}