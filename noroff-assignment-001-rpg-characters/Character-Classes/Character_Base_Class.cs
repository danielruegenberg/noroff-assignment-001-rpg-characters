﻿namespace noroff_assignment_001_rpg_characters.Character_Classes
{
    abstract public class Character_Base_Class
    {
        private string characterName;
        private int totalLevel;
        private int startingLevel = 1;
        private string className;
        private string primaryAttribute;
        private int baseStrength;
        private int baseDexterity;
        private int baseIntelligence;
        private int lvlUpStrength;
        private int lvlUpDexterity;
        private int lvlUpIntelligence;
        private int totalStrength;
        private int totalDexterity;
        private int totalIntelligence;
        private double characterDamage = 1.0;
        private string headSlot;
        private string bodySlot;
        private string legsSlot;
        private string weaponSlot;
        private int headSlotModifier;
        private int bodySlotModifier;
        private int legsSlotModifier;
        private double weaponSlotDPS = 1;
        public string CharacterName { get => characterName; set => characterName = value; }
        public int TotalLevel { get => totalLevel; set => totalLevel = value; }
        public int StartingLevel { get => startingLevel; set => startingLevel = value; }
        public string ClassName { get => className; set => className = value; }
        public string PrimaryAttribute { get => primaryAttribute; set => primaryAttribute = value; }
        public int BaseStrength { get => baseStrength; set => baseStrength = value; }
        public int BaseDexterity { get => baseDexterity; set => baseDexterity = value; }
        public int BaseIntelligence { get => baseIntelligence; set => baseIntelligence = value; }
        public int LvlUpStrength { get => lvlUpStrength; set => lvlUpStrength = value; }
        public int LvlUpDexterity { get => lvlUpDexterity; set => lvlUpDexterity = value; }
        public int LvlUpIntelligence { get => lvlUpIntelligence; set => lvlUpIntelligence = value; }
        public int TotalStrength { get => totalStrength; set => totalStrength = value; }
        public int TotalDexterity { get => totalDexterity; set => totalDexterity = value; }
        public int TotalIntelligence { get => totalIntelligence; set => totalIntelligence = value; }
        public string HeadSlot { get => headSlot; set => headSlot = value; }
        public string BodySlot { get => bodySlot; set => bodySlot = value; }
        public string LegsSlot { get => legsSlot; set => legsSlot = value; }
        public string WeaponSlot { get => weaponSlot; set => weaponSlot = value; }
        public int HeadSlotModifier { get => headSlotModifier; set => headSlotModifier = value; }
        public int BodySlotModifier { get => bodySlotModifier; set => bodySlotModifier = value; }
        public int LegsSlotModifier { get => legsSlotModifier; set => legsSlotModifier = value; }
        public double CharacterDamage { get => characterDamage; set => characterDamage = value; }
        public double WeaponSlotDPS { get => weaponSlotDPS; set => weaponSlotDPS = value; }
    }
}
