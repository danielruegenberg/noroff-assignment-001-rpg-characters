﻿using noroff_assignment_001_rpg_characters.Custom_Exceptions;
using System.Collections.Generic;

namespace noroff_assignment_001_rpg_characters.Character_Classes
{
    abstract public class Create_Hero_Class : Character_Base_Class
    {
        private string basicClassName;
        private List<string> usableArmor = new List<string>();
        private List<string> usableWeapons = new List<string>();
        public string BasicClassName { get => basicClassName; set => basicClassName = value; }
        public List<string> UsableArmor { get => usableArmor; set => usableArmor = value; }
        public List<string> UsableWeapons { get => usableWeapons; set => usableWeapons = value; }
        /// <summary>
        /// Takes in the name of the hero class to be created and sets up all the needed values. If an invalid class is provided, an exception is thrown
        /// </summary>
        /// <exception cref="InvalidHeroClassException"></exception>
        public void SetBaseStats()
        {
            switch ( basicClassName )
            {
                case "mage":                   
                    base.ClassName = "Mage";
                    base.PrimaryAttribute = "Intelligence";
                    base.BaseStrength = 1;
                    base.BaseDexterity = 1;
                    base.BaseIntelligence = 8;
                    base.LvlUpStrength = 1;
                    base.LvlUpDexterity = 1;
                    base.LvlUpIntelligence = 5;
                    base.TotalStrength = base.BaseStrength;
                    base.TotalDexterity = base.BaseDexterity;
                    base.TotalIntelligence = base.BaseIntelligence;
                    usableArmor.Add("cloth");
                    usableWeapons.Add("staff");
                    usableWeapons.Add("wand");
                    break;
                case "ranger":
                    base.ClassName = "Ranger";
                    base.PrimaryAttribute = "Dexterity";
                    base.BaseStrength = 1;
                    base.BaseDexterity = 7;
                    base.BaseIntelligence = 1;
                    base.LvlUpStrength = 1;
                    base.LvlUpDexterity = 5;
                    base.LvlUpIntelligence = 1;
                    base.TotalStrength = base.BaseStrength;
                    base.TotalDexterity = base.BaseDexterity;
                    base.TotalIntelligence = base.BaseIntelligence;
                    usableArmor.Add("leather");
                    usableArmor.Add("mail");
                    usableWeapons.Add("bow");
                    break;
                case "rouge":
                    base.ClassName = "Rouge";
                    base.PrimaryAttribute = "Dexterity";
                    base.BaseStrength = 2;
                    base.BaseDexterity = 6;
                    base.BaseIntelligence = 1;
                    base.LvlUpStrength = 1;
                    base.LvlUpDexterity = 4;
                    base.LvlUpIntelligence = 1;
                    base.TotalStrength = base.BaseStrength;
                    base.TotalDexterity = base.BaseDexterity;
                    base.TotalIntelligence = base.BaseIntelligence;
                    usableArmor.Add("leather");
                    usableArmor.Add("mail");
                    usableWeapons.Add("dagger");
                    usableWeapons.Add("sword");
                    break;
                case "warrior":
                    base.ClassName = "Warrior";
                    base.PrimaryAttribute = "Strength";
                    base.BaseStrength = 5;
                    base.BaseDexterity = 2;
                    base.BaseIntelligence = 1;
                    base.LvlUpStrength = 3;
                    base.LvlUpDexterity = 2;
                    base.LvlUpIntelligence = 1;
                    base.TotalStrength = base.BaseStrength;
                    base.TotalDexterity = base.BaseDexterity;
                    base.TotalIntelligence = base.BaseIntelligence;
                    usableArmor.Add("mail");
                    usableArmor.Add("plate");
                    usableWeapons.Add("axe");
                    usableWeapons.Add("hammer");
                    usableWeapons.Add("sword");
                    break;
                default:
                    throw new InvalidHeroClassException($"The hero class '{ basicClassName }' does not exist");
            }
        }        
    }
}
