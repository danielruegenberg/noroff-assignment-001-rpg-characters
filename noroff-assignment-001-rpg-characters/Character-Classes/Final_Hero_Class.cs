﻿using System;
using noroff_assignment_001_rpg_characters.Equipment_Classes;

namespace noroff_assignment_001_rpg_characters.Character_Classes
{
    public class Final_Hero_Class : Create_Hero_Class
    {
        public Final_Hero_Class(string nameOfCharacter, string nameOfClass)
        {
            base.CharacterName = nameOfCharacter;
            base.BasicClassName = nameOfClass.ToLower();
            base.TotalLevel = base.StartingLevel;
            base.SetBaseStats();
        }
        /// <summary>
        /// Updates the total attributes of a hero acording to the new level and calls the CalculateCharacterDamage() method
        /// </summary>
        public void LevelUp()
        {
            base.TotalLevel++;
            base.TotalStrength += base.LvlUpStrength;
            base.TotalDexterity += base.LvlUpDexterity;
            base.TotalIntelligence += base.LvlUpIntelligence;
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Takes in the name of the armor to be equipped at the head slot and calls the EquipArmor() method. After that the FindPrimaryBaseStatAndUpdateStats() method is called
        /// </summary>
        /// <param name="nameOfArmor"></param>
        public void EquipHead(string nameOfArmor)
        {
            Armor_Class myArmorClassInstance = new Armor_Class();
            var armorModifier = myArmorClassInstance.EquipArmor(nameOfArmor.ToLower(), base.TotalLevel, base.UsableArmor, "headSlot");
            int oldModifier = base.HeadSlotModifier;
            base.HeadSlotModifier = armorModifier.armorPower;
            if (base.HeadSlotModifier == 0)
            {
                base.HeadSlot = "";
            }
            else
            {
                base.HeadSlot = armorModifier.armorName;
            }
            FindPrimaryBaseStatAndUpdateStats(oldModifier, base.HeadSlotModifier);
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Removes the head armor modifier from the heros primary attribute. To do so, the base.HeadSlotModifier of the armor is inverted to a negative number 
        /// and it is then added to the primary attribute value using the FindPrimaryBaseStatAndUpdateStats() method.
        /// At last the base.HeadSlotModifier is set to zero
        /// </summary>
        public void UnequipHead()
        {
            if (base.HeadSlotModifier != 0)
            {
                int oldModifier = 0;
                base.HeadSlot = "";
                base.HeadSlotModifier *= -1;
                FindPrimaryBaseStatAndUpdateStats(oldModifier, base.HeadSlotModifier);
                CalculateCharacterDamage();
                base.HeadSlotModifier = 0;
            }
        }
        /// <summary>
        /// Takes in the name of the armor to be equipped at the body slot and calls the EquipArmor() method. After that the FindPrimaryBaseStatAndUpdateStats() method is called
        /// </summary>
        /// <param name="nameOfArmor"></param>
        public void EquipBody(string nameOfArmor)
        {
            Armor_Class myArmorClassInstance = new Armor_Class();
            var armorModifier = myArmorClassInstance.EquipArmor(nameOfArmor.ToLower(), base.TotalLevel, base.UsableArmor, "bodySlot");
            int oldModifier = base.BodySlotModifier;
            base.BodySlotModifier = armorModifier.armorPower;
            if (base.BodySlotModifier == 0)
            {
                base.BodySlot = "";
            }
            else
            {
                base.BodySlot = armorModifier.armorName;
            }
            FindPrimaryBaseStatAndUpdateStats(oldModifier, base.BodySlotModifier);
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Removes the body armor modifier from the heros primary attribute. To do so, the base.BodySlotModifier of the armor is inverted to a negative number 
        /// and it is then added to the primary attribute value using the FindPrimaryBaseStatAndUpdateStats() method.
        /// At last the base.BodySlotModifier is set to zero 
        /// </summary>
        public void UnequipBody()
        {
            int oldModifier = 0;
            base.BodySlot = "";
            base.BodySlotModifier *= -1;
            FindPrimaryBaseStatAndUpdateStats(oldModifier, base.BodySlotModifier);
            CalculateCharacterDamage();
            base.BodySlotModifier = 0;
        }
        /// <summary>
        /// Takes in the name of the armor to be equipped at the legs slot and calls the EquipArmor() method. After that the FindPrimaryBaseStatAndUpdateStats() method is called
        /// </summary>
        /// <param name="nameOfArmor"></param>
        public void EquipLegs(string nameOfArmor)
        {
            Armor_Class myArmorClassInstance = new Armor_Class();
            var armorModifier = myArmorClassInstance.EquipArmor(nameOfArmor.ToLower(), base.TotalLevel, base.UsableArmor, "legsSlot");
            int oldModifier = base.LegsSlotModifier;
            base.LegsSlotModifier = armorModifier.armorPower;
            if (base.LegsSlotModifier == 0)
            {
                base.LegsSlot = "";
            }
            else
            {
                base.LegsSlot = armorModifier.armorName;
            }
            FindPrimaryBaseStatAndUpdateStats(oldModifier, base.LegsSlotModifier);
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Removes the legs armor modifier from the heros primary attribute. To do so, the base.LegsSlotModifier of the armor is inverted to a negative number 
        /// and it is then added to the primary attribute value using the FindPrimaryBaseStatAndUpdateStats() method.
        /// At last the base.LegsSlotModifier is set to zero 
        /// </summary>
        public void UnequipLegs()
        {
            int oldModifier = 0;
            base.LegsSlot = "";
            base.LegsSlotModifier *= -1;
            FindPrimaryBaseStatAndUpdateStats(oldModifier, base.LegsSlotModifier);
            CalculateCharacterDamage();
            base.LegsSlotModifier = 0;
        }
        /// <summary>
        /// Takes in the name of the weapon to be equipped at the weapon slot and calls the EquipArmor() method. After that the FindPrimaryBaseStatAndUpdateStats() method is called
        /// </summary>
        /// <param name="nameOfWeapon"></param>
        public void EquipWeapon(string nameOfWeapon)
        {
            Weapon_Class myWeaponClassInstance = new Weapon_Class();
            var weaponModifier = myWeaponClassInstance.EquipWeapon(nameOfWeapon.ToLower(), base.TotalLevel, base.UsableWeapons);
            if (weaponModifier.weaponPower == 0)
            {
                base.WeaponSlot = "";
                base.WeaponSlotDPS = 1;
                CalculateCharacterDamage();
            }
            else
            {
                base.WeaponSlot = weaponModifier.weaponName;
                CalculateDPS(weaponModifier.weaponPower, weaponModifier.weaponAttackSpeed);
                CalculateCharacterDamage();
            }
        }
        /// <summary>
        /// Resets the base.WeaponSlot to an empty string, the base.WeaponSlotDPS to zero and the base.CharacterDamage to 1
        /// </summary>
        public void UnequipWeapon()
        {
            base.WeaponSlot = "";
            base.WeaponSlotDPS = 0;
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Finds the primary attribute of the hero and updates it with the new modifier value. Then the CalculateCharacterDamage() method is called
        /// </summary>
        /// <param name="oldModifier"></param>
        /// <param name="newModifier"></param>
        private void FindPrimaryBaseStatAndUpdateStats(int oldModifier, int newModifier)
        {
            if (base.BaseStrength > base.BaseDexterity)
            {
                if (base.BaseStrength > base.BaseIntelligence)
                {
                    base.TotalStrength -= oldModifier;
                    base.TotalStrength += newModifier;
                }
                else
                {
                    base.TotalIntelligence -= oldModifier;
                    base.TotalIntelligence += newModifier;
                }
            }
            else
            {
                if (base.BaseDexterity > base.BaseIntelligence)
                {
                    base.TotalDexterity -= oldModifier;
                    base.TotalDexterity += newModifier;
                }
                else
                {
                    base.TotalIntelligence -= oldModifier;
                    base.TotalIntelligence += newModifier;
                }
            }
            CalculateCharacterDamage();
        }
        /// <summary>
        /// Calculates the weapon DPS by multiplying the weaponPower with the weaponAttackSpeed
        /// </summary>
        /// <param name="weaponPower"></param>
        /// <param name="weaponAttackSpeed"></param>
        private void CalculateDPS(int weaponPower, double weaponAttackSpeed)
        {
            double weaponDPS = weaponPower * weaponAttackSpeed;
            double roundedWeaponDPS = Math.Round(weaponDPS, 1);
            base.WeaponSlotDPS = roundedWeaponDPS;
        }
        /// <summary>
        /// Checks if the weapon slot is equipped by looking at the current base.WeaponSlotDPS
        /// If it is zero, the base.CharacterDamage is set to 1
        /// Else it finds the primary Attribute of the hero, calculates the damage and updates the base.CharacterDamage
        /// </summary>
        private void CalculateCharacterDamage()
        {
            if (base.BaseStrength > base.BaseDexterity)
            {
                if (base.BaseStrength > base.BaseIntelligence)
                {
                    double baseIntValueToDouble = Convert.ToDouble(base.TotalStrength);
                    double divisor = 100;
                    double weaponDPS = base.WeaponSlotDPS;
                    double characterDamage = weaponDPS * (1 + baseIntValueToDouble / divisor);
                    double roundedCharacterDamage = Math.Round(characterDamage, 1);
                    base.CharacterDamage = roundedCharacterDamage;
                }
                else
                {
                    double baseIntValueToDouble = Convert.ToDouble(base.TotalIntelligence);
                    double divisor = 100;
                    double weaponDPS = base.WeaponSlotDPS;
                    double characterDamage = weaponDPS * (1 + baseIntValueToDouble / divisor);
                    double roundedCharacterDamage = Math.Round(characterDamage, 1);
                    base.CharacterDamage = roundedCharacterDamage;
                }
            }
            else
            {
                if (base.BaseDexterity > base.BaseIntelligence)
                {
                    double baseIntValueToDouble = Convert.ToDouble(base.TotalDexterity);
                    double divisor = 100;
                    double weaponDPS = base.WeaponSlotDPS;
                    double characterDamage = weaponDPS * (1 + baseIntValueToDouble / divisor);
                    double roundedCharacterDamage = Math.Round(characterDamage, 1);
                    base.CharacterDamage = roundedCharacterDamage;
                }
                else
                {
                    double baseIntValueToDouble = Convert.ToDouble(base.TotalIntelligence);
                    double divisor = 100;
                    double weaponDPS = base.WeaponSlotDPS;
                    double characterDamage = weaponDPS * (1 + baseIntValueToDouble / divisor);
                    double roundedCharacterDamage = Math.Round(characterDamage, 1);
                    base.CharacterDamage = roundedCharacterDamage;
                }
            }         
        }
        /// <summary>
        /// Prints out the basic stats of the hero to the console
        /// </summary>
        public void PrintBasicStats()
        {
            Console.WriteLine($"Name: {base.CharacterName}");
            Console.WriteLine($"Class: {base.ClassName}");
            Console.WriteLine($"Level: {base.TotalLevel}");
            Console.WriteLine($"Attack Damage: {base.CharacterDamage}");
            Console.WriteLine($"Strenght: {base.TotalStrength}");
            Console.WriteLine($"Dexterity: {base.TotalDexterity}");
            Console.WriteLine($"Intelligence: {base.TotalIntelligence}");
            Console.WriteLine($"Head equipment: {base.HeadSlot}");
            Console.WriteLine($"Body equipment: {base.BodySlot}");
            Console.WriteLine($"Legs equipment: {base.LegsSlot}");
            Console.WriteLine($"Weapon equipment: {base.WeaponSlot}");
            Console.WriteLine("");
        }
        /// <summary>
        /// Prints out the verbose stats of the hero to the console
        /// </summary>
        public void PrintVerboseStats()
        {
            Console.WriteLine($"Name: {base.CharacterName}");
            Console.WriteLine($"Class: {base.ClassName}");
            Console.WriteLine($"Total Level: {base.TotalLevel}");
            Console.WriteLine($"Total Attack Damage: {base.CharacterDamage}");
            Console.WriteLine($"Total Strenght: {base.TotalStrength}");
            Console.WriteLine($"Total Dexterity: {base.TotalDexterity}");
            Console.WriteLine($"Total Intelligence: {base.TotalIntelligence}");
            Console.WriteLine($"Primary Attribute: {base.PrimaryAttribute}");
            Console.WriteLine($"Starting Level: {base.StartingLevel}");
            Console.WriteLine($"Basic Strenght: {base.BaseStrength}");
            Console.WriteLine($"Basic Dexterity: {base.BaseDexterity}");
            Console.WriteLine($"Basic Intelligence: {base.BaseIntelligence}");
            Console.WriteLine($"Level up Strenght modifier: {base.LvlUpStrength}");
            Console.WriteLine($"Level up Dexterity modifier: {base.LvlUpDexterity}");
            Console.WriteLine($"Level up Intelligence modifier: {base.LvlUpIntelligence}");
            Console.WriteLine($"Head equipment: {base.HeadSlot}");
            Console.WriteLine($"Head equipment modifier: {base.HeadSlotModifier}");
            Console.WriteLine($"Body equipment: {base.BodySlot}");
            Console.WriteLine($"Body equipment modifier: {base.BodySlotModifier}");
            Console.WriteLine($"Legs equipment: {base.LegsSlot}");
            Console.WriteLine($"Legs equipment modifier: {base.LegsSlotModifier}");
            Console.WriteLine($"Weapon equipment: {base.WeaponSlot}");
            Console.WriteLine($"Weapon equipment DPS: {base.WeaponSlotDPS}");
            foreach (string armor in base.UsableArmor)
            {
                Console.WriteLine($"Can use armor type: {armor}");
            }
            foreach (string weapon in base.UsableWeapons)
            {
                Console.WriteLine($"Can use weapon type: {weapon}");
            }
            Console.WriteLine("");
        }
    }
}
