﻿using noroff_assignment_001_rpg_characters.Character_Classes;
using System;

namespace noroff_assignment_001_rpg_characters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //If you want to try out the usage example, uncomment below regioned rows
                #region
                //Final_Hero_Class testRangerHero = new Final_Hero_Class("testRangerHero", "ranger");
                //testRangerHero.EquipLegs("old leather pants");
                //testRangerHero.LevelUp();
                //testRangerHero.LevelUp();
                //testRangerHero.EquipBody("mediocre mail coat");
                //testRangerHero.EquipHead("mediocre leather hat");
                //testRangerHero.LevelUp();
                //testRangerHero.LevelUp();
                //testRangerHero.EquipWeapon("new bow");
                //testRangerHero.PrintBasicStats();
                //testRangerHero.PrintVerboseStats();
                #endregion
            }
            catch (Exception ex)
            #region
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.GetType().Name);
                Console.WriteLine("");
                Console.WriteLine(ex.Message.ToString());
                Console.WriteLine("");
                Console.WriteLine(ex.StackTrace.ToString());
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.White;
            }
            #endregion
        }
    }
}
